#include <stdint.h>

void ssp2_hw_init(uint32_t max_clock_mhz);

uint8_t ssp2_hw_exchange_byte(uint8_t data_out);

uint8_t ssp2_hw_sr(uint8_t sr_data);