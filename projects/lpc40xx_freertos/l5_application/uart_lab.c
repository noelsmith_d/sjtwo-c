

#include "uart_lab.h"

#include "FreeRTOS.h"
#include "lpc40xx.h"
#include "lpc_peripherals.h"
#include "queue.h"
#include <stdio.h>

// The idea is that you will use interrupts to input data to FreeRTOS queue
// Then, instead of polling, your tasks can sleep on data that we can read from the queue

static QueueHandle_t uart_rx_queue;
static int uart_rx_queue_size = 10;

// Private function of our uart_lab.c
static void uart_lab__receive_interrupt_uart2(void) {
  const uint32_t iir_intid_receive_data_available = 0x2;
  const uint32_t lcr_dlab_disable = ~(1 << 7);
  const uint32_t lsr_receiver_data_ready_bit = (1 << 0);

  // Read the IIR register to figure out why you got interrupted
  uint32_t interrupt_identification_value = (LPC_UART2->IIR >> 1) & 0x7;

  // Based on IIR status, read the LSR register to confirm if there is data to be read
  // Based on LSR status, read the RBR register and input the data to the RX Queue
  if (interrupt_identification_value == iir_intid_receive_data_available) {
    if (LPC_UART2->LSR & lsr_receiver_data_ready_bit) {
      LPC_UART2->LCR &= lcr_dlab_disable;
      const char received_byte = LPC_UART2->RBR;
      xQueueSendFromISR(uart_rx_queue, &received_byte, NULL);
    } else {
      fprintf(stderr, "Interrupt Identification Register (IIR) Interrupt Identification (INTID) was Receive Data "
                      "Available (RDA) value, but Line Status Register (LSR) showed RDR was not ready");
    }
  } else {
    fprintf(stderr, "Interrupt Identification Register (IIR) interrupt identification value %ld not handled",
            interrupt_identification_value);
  }
}

static void uart_lab__receive_interrupt_uart3(void) {
  const uint32_t iir_intid_receive_data_available = 0x2;
  const uint32_t lcr_dlab_disable = ~(1 << 7);
  const uint32_t lsr_receiver_data_ready_bit = (1 << 0);

  // Read the IIR register to figure out why you got interrupted
  uint32_t interrupt_identification_value = (LPC_UART3->IIR >> 1) & 0x7;

  // Based on IIR status, read the LSR register to confirm if there is data to be read
  // Based on LSR status, read the RBR register and input the data to the RX Queue
  if (interrupt_identification_value == iir_intid_receive_data_available) {
    if (LPC_UART3->LSR & lsr_receiver_data_ready_bit) {
      LPC_UART3->LCR &= lcr_dlab_disable;
      const char received_byte = LPC_UART3->RBR;
      xQueueSendFromISR(uart_rx_queue, &received_byte, NULL);
    } else {
      fprintf(stderr, "Interrupt Identification Register (IIR) Interrupt Identification (INTID) was Receive Data "
                      "Available (RDA) value, but Line Status Register (LSR) showed RDR was not ready");
    }
  } else {
    fprintf(stderr, "Interrupt Identification Register (IIR) interrupt identification value %ld not handled",
            interrupt_identification_value);
  }
}

void uart_lab__enable_receive_interrupt(uart_number_e uart_number) {
  const uint32_t lcr_dlab_disable = ~(1 << 7);
  const uint32_t rbr_interrupt_enable = (1 << 0);

  if (uart_number == UART_2) {
    lpc_peripheral__enable_interrupt(LPC_PERIPHERAL__UART2, uart_lab__receive_interrupt_uart2, NULL);
    LPC_UART2->LCR &= lcr_dlab_disable;
    LPC_UART2->IER |= rbr_interrupt_enable;
  } else if (uart_number == UART_3) {
    lpc_peripheral__enable_interrupt(LPC_PERIPHERAL__UART3, uart_lab__receive_interrupt_uart3, NULL);
    LPC_UART3->LCR &= lcr_dlab_disable;
    LPC_UART3->IER |= rbr_interrupt_enable;
  } else {
    fprintf(stderr, "UART %d not handled\n", uart_number);
    return;
  }

  uart_rx_queue = xQueueCreate(uart_rx_queue_size, sizeof(char));
}

bool uart_lab__get_char_from_queue(char *input_byte, uint32_t timeout) {
  return xQueueReceive(uart_rx_queue, input_byte, timeout);
}

void uart_lab__init(uart_number_e uart, uint32_t peripheral_clock, uint32_t baud_rate) {
  // Refer to LPC User manual and setup the register bits correctly
  // The first page of the UART chapter has good instructions
  // a) Power on Peripheral
  // b) Setup DLL, DLM, FDR, LCR registers

  const uint32_t uart2_power_enable = (1 << 24);
  const uint32_t uart3_power_enable = (1 << 25);
  const uint32_t lcr_dlab_enable = (1 << 7);
  const uint32_t lcr_clear_word_length_select_bits = ~(3 << 0);
  const uint32_t lcr_word_length_select_8_bits = (3 << 0);

  const uint16_t divider_16_bit = (uint16_t)(peripheral_clock / (16 * baud_rate));

  if (uart == UART_2) {
    LPC_SC->PCONP |= uart2_power_enable;
    LPC_UART2->LCR |= lcr_dlab_enable;
    LPC_UART2->DLM = (divider_16_bit >> 8) & 0xFF;
    LPC_UART2->DLL = (divider_16_bit >> 0) & 0xFF;
    LPC_UART2->LCR &= ~lcr_dlab_enable;
    // Set 8-bit transfer
    LPC_UART2->LCR &= lcr_clear_word_length_select_bits;
    LPC_UART2->LCR |= lcr_word_length_select_8_bits;
  } else if (uart == UART_3) {
    LPC_SC->PCONP |= uart3_power_enable;
    LPC_UART3->LCR |= lcr_dlab_enable;
    LPC_UART3->DLM = (divider_16_bit >> 8) & 0xFF;
    LPC_UART3->DLL = (divider_16_bit >> 0) & 0xFF;
    LPC_UART3->LCR &= ~lcr_dlab_enable;
    // Set 8-bit transfer
    LPC_UART3->LCR &= lcr_clear_word_length_select_bits;
    LPC_UART3->LCR |= lcr_word_length_select_8_bits;
  } else {
    fprintf(stderr, "UART %d not handled\n", uart);
  }
}

// Read the byte from RBR and actually save it to the pointer
bool uart_lab__polled_get(uart_number_e uart, char *input_byte) {
  // a) Check LSR for Receive Data Ready
  // b) Copy data from RBR register to input_byte

  const uint32_t lcr_dlab_disable = ~(1 << 7);
  const uint32_t lsr_receiver_data_ready_bit = (1 << 0);

  if (uart == UART_2) {
    while (!(LPC_UART2->LSR & lsr_receiver_data_ready_bit)) {
      // poll until receiver buffer register is not empty
    }
    LPC_UART2->LCR &= lcr_dlab_disable;
    *input_byte = LPC_UART2->RBR;
  } else if (uart == UART_3) {
    while (!(LPC_UART3->LSR & lsr_receiver_data_ready_bit)) {
      // poll until receiver buffer register is not empty
    }
    LPC_UART3->LCR &= lcr_dlab_disable;
    *input_byte = LPC_UART3->RBR;
  } else {
    fprintf(stderr, "UART %d not handled\n", uart);
    return false;
  }
  return true;
}

bool uart_lab__polled_put(uart_number_e uart, char output_byte) {
  // a) Check LSR for Transmit Hold Register Empty
  // b) Copy output_byte to THR register

  const uint32_t lcr_dlab_disable = ~(1 << 7);
  const uint32_t lsr_transmitter_holding_register_empty_bit = (1 << 5);

  if (uart == UART_2) {
    while (!(LPC_UART2->LSR & lsr_transmitter_holding_register_empty_bit)) {
      // poll until transmitter holding register is empty
    }
    LPC_UART2->LCR &= lcr_dlab_disable;
    LPC_UART2->THR = output_byte;
  } else if (uart == UART_3) {
    while (!(LPC_UART3->LSR & lsr_transmitter_holding_register_empty_bit)) {
      // poll until transmitter holding register is empty
    }
    LPC_UART3->LCR &= lcr_dlab_disable;
    LPC_UART3->THR = output_byte;
  } else {
    fprintf(stderr, "UART %d not handled\n", uart);
    return false;
  }
  return true;
}
