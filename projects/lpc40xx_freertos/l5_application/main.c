

#include "FreeRTOS.h"
#include "task.h"
#include <stdio.h>

#include "uart_lab.h"

//#define part1_loopback_test
#define part2_receive_with_interrupts
//#define part3_interface_two_sj_boards_with_uart

#ifdef part1_loopback_test
void uart_read_task(void *p) {
  puts("Starting read task");
  while (1) {
    char input_byte = '0';
    if (uart_lab__polled_get(UART_3, &input_byte)) {
      printf("Received: %c\n", input_byte);
    } else {
      puts("uart_lab__polled_get returned false");
    }
    vTaskDelay(500);
  }
}

void uart_write_task(void *p) {
  puts("Starting write task");
  while (1) {
    char output_byte = 'a';
    if (uart_lab__polled_put(UART_3, output_byte)) {
      printf("Sent: %c\n", output_byte);
    } else {
      puts("uart_lab__polled_put returned false");
    }
    vTaskDelay(500);
  }
}

int main(void) {
  const uint32_t peripheral_clock = clock__get_peripheral_clock_hz();
  const uint32_t baud_rate = 115200;
  const uint32_t iocon_clear_function_bits = ~(7 << 0);
  const uint32_t iocon_u2_txd_rxd_enable = (1 << 0);
  const uint32_t iocon_u3_txd_rxd_enable = (2 << 0);

  puts("Starting UART lab");
  uart_lab__init(UART_2, peripheral_clock, baud_rate);
  uart_lab__init(UART_3, peripheral_clock, baud_rate);
  puts("UART initialized");

  // Set function of GPIO pins to UART
  LPC_GPIO0->DIR |= (1 << 10);
  LPC_GPIO0->DIR &= ~(1 << 11);
  LPC_IOCON->P0_10 &= iocon_clear_function_bits;
  LPC_IOCON->P0_10 |= iocon_u2_txd_rxd_enable;
  LPC_IOCON->P0_11 &= iocon_clear_function_bits;
  LPC_IOCON->P0_11 |= iocon_u2_txd_rxd_enable;
  // Set function of GPIO pin to UART
  LPC_GPIO0->DIR |= (1 << 0);
  LPC_GPIO0->DIR &= ~(1 << 1);
  LPC_IOCON->P0_0 &= iocon_clear_function_bits;
  LPC_IOCON->P0_0 |= iocon_u3_txd_rxd_enable;
  LPC_IOCON->P0_1 &= iocon_clear_function_bits;
  LPC_IOCON->P0_1 |= iocon_u3_txd_rxd_enable;

  xTaskCreate(&uart_read_task, "uart_read_task", 1024, NULL, PRIORITY_LOW, NULL);
  xTaskCreate(&uart_write_task, "uart_write_task", 1024, NULL, PRIORITY_LOW, NULL);

  vTaskStartScheduler();

  return 0;
}
#endif

#ifdef part2_receive_with_interrupts
#include <stdlib.h>

void uart_read_task(void *p) {
  puts("Starting read task");
  while (1) {
    char input_byte = '0';
    if (uart_lab__get_char_from_queue(&input_byte, portMAX_DELAY)) {
      printf("Received from Interrupt: %c\n", input_byte);
    } else {
      puts("uart_lab__get_char_from_queue timed out");
    }
    vTaskDelay(500);
  }
}

void uart_write_task(void *p) {
  puts("Starting write task");
  while (1) {
    char output_byte = 'a';
    if (uart_lab__polled_put(UART_3, output_byte)) {
      printf("Sent: %c\n", output_byte);
    } else {
      puts("uart_lab__polled_put returned false");
    }
    vTaskDelay(500);
  }
}

int main(void) {
  const uint32_t peripheral_clock = clock__get_peripheral_clock_hz();
  const uint32_t baud_rate = 9600;
  const uint32_t iocon_clear_function_bits = ~(7 << 0);
  const uint32_t iocon_u2_txd_rxd_enable = (1 << 0);
  const uint32_t iocon_u3_txd_rxd_enable = (2 << 0);

  puts("Starting UART lab");
  uart_lab__init(UART_2, peripheral_clock, baud_rate);
  uart_lab__init(UART_3, peripheral_clock, baud_rate);
  puts("UART initialized");

  // Set function of GPIO pins to UART
  LPC_GPIO0->DIR |= (1 << 10);
  LPC_GPIO0->DIR &= ~(1 << 11);
  LPC_IOCON->P0_10 &= iocon_clear_function_bits;
  LPC_IOCON->P0_10 |= iocon_u2_txd_rxd_enable;
  LPC_IOCON->P0_11 &= iocon_clear_function_bits;
  LPC_IOCON->P0_11 |= iocon_u2_txd_rxd_enable;

  // Set function of GPIO pin to UART
  LPC_GPIO0->DIR |= (1 << 0);
  LPC_GPIO0->DIR &= ~(1 << 1);
  LPC_IOCON->P0_0 &= iocon_clear_function_bits;
  LPC_IOCON->P0_0 |= iocon_u3_txd_rxd_enable;
  LPC_IOCON->P0_1 &= iocon_clear_function_bits;
  LPC_IOCON->P0_1 |= iocon_u3_txd_rxd_enable;

  uart_lab__enable_receive_interrupt(UART_2);
  uart_lab__enable_receive_interrupt(UART_3);

  xTaskCreate(&uart_read_task, "uart_read_task", 1024, NULL, PRIORITY_LOW, NULL);
  xTaskCreate(&uart_write_task, "uart_write_task", 1024, NULL, PRIORITY_LOW, NULL);

  vTaskStartScheduler();

  return 0;
}
#endif

#ifdef part3_interface_two_sj_boards_with_uart
#include <stdlib.h>
#include <string.h>

void board_1_sender_task(void *p) {
  char number_as_string[16] = {0};

  while (true) {
    const int number = rand();
    sprintf(number_as_string, "%i", number);

    // Send one char at a time to the other board including terminating NULL char
    for (int i = 0; i <= strlen(number_as_string); i++) {
      uart_lab__polled_put(UART_3, number_as_string[i]);
      printf("Sent: %c\n", number_as_string[i]);
    }

    printf("Sent: %i over UART to the other board\n", number);
    vTaskDelay(3000);
  }
}

void board_2_receiver_task(void *p) {
  char number_as_string[16] = {0};
  int counter = 0;

  while (true) {
    char byte = 0;
    uart_lab__get_char_from_queue(&byte, portMAX_DELAY);
    printf("Received over from another board: %c\n", byte);

    // This is the last char, so print the number
    if ('\0' == byte) {
      number_as_string[counter] = '\0';
      counter = 0;
      printf("Received this number from the other board: %s\n", number_as_string);
    }
    // We have not yet received the NULL '\0' char, so buffer the data
    else {
      number_as_string[counter] = byte;
      counter++;
    }
  }
}

int main(void) {
  const uint32_t peripheral_clock = clock__get_peripheral_clock_hz();
  const uint32_t baud_rate = 9600;
  const uint32_t iocon_clear_function_bits = ~(7 << 0);
  const uint32_t iocon_u2_txd_rxd_enable = (1 << 0);
  const uint32_t iocon_u3_txd_rxd_enable = (2 << 0);

  puts("Starting UART lab");
  uart_lab__init(UART_2, peripheral_clock, baud_rate);
  uart_lab__init(UART_3, peripheral_clock, baud_rate);
  puts("UART initialized");

  // Set function of GPIO pins to UART
  LPC_GPIO0->DIR |= (1 << 10);
  LPC_GPIO0->DIR &= ~(1 << 11);
  LPC_IOCON->P0_10 &= iocon_clear_function_bits;
  LPC_IOCON->P0_10 |= iocon_u2_txd_rxd_enable;
  LPC_IOCON->P0_11 &= iocon_clear_function_bits;
  LPC_IOCON->P0_11 |= iocon_u2_txd_rxd_enable;
  // Set function of GPIO pin to UART
  LPC_GPIO0->DIR |= (1 << 0);
  LPC_GPIO0->DIR &= ~(1 << 1);
  LPC_IOCON->P0_0 &= iocon_clear_function_bits;
  LPC_IOCON->P0_0 |= iocon_u3_txd_rxd_enable;
  LPC_IOCON->P0_1 &= iocon_clear_function_bits;
  LPC_IOCON->P0_1 |= iocon_u3_txd_rxd_enable;

  uart_lab__enable_receive_interrupt(UART_2);
  uart_lab__enable_receive_interrupt(UART_3);

  xTaskCreate(&board_1_sender_task, "board_1_sender_task", 1024, NULL, PRIORITY_LOW, NULL);
  xTaskCreate(&board_2_receiver_task, "board_2_receiver_task", 1024, NULL, PRIORITY_LOW, NULL);

  vTaskStartScheduler();

  return 0;
}
#endif
