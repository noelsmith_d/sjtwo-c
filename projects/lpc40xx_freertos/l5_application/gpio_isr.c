
#include "gpio_isr.h"
#include "lpc40xx.h"
#include "stdio.h"
#include <stdbool.h>

// Note: You may want another separate array for falling vs. rising edge callbacks
static function_pointer_t gpio0_rising_edge_callbacks[32];
static function_pointer_t gpio0_falling_edge_callbacks[32];

void gpio0__attach_interrupt(uint32_t pin, gpio_interrupt_e interrupt_type, function_pointer_t callback) {
  // 1) Store the callback based on the pin at gpio0_callbacks
  // 2) Configure GPIO 0 pin for rising or falling edge
  if (interrupt_type == GPIO_INTR__FALLING_EDGE) {
    gpio0_falling_edge_callbacks[pin] = callback; // store callback function to function pointer
    LPC_GPIOINT->IO0IntEnF |= (1 << pin);         // configure to trigger interrupt
  } else if (interrupt_type == GPIO_INTR__RISING_EDGE) {
    gpio0_rising_edge_callbacks[pin] = callback;
    LPC_GPIOINT->IO0IntEnR |= (1 << pin);
  } else {
    fprintf(stderr, "GPIO Interrupt Type invalid\n");
  }
}

int sw_pin_that_generated_interrupt(void) {

  for (int i = 0; i < 32; i++) {
    // Check which pin generated the interrupt
    if ((LPC_GPIOINT->IO0IntStatR & (1 << i))) {
      int pin_that_generated_interrupt = i;
      return i;
    } else if ((LPC_GPIOINT->IO0IntStatF & (1 << i))) {
      int pin_that_generated_interrupt = i;
      return i;
    }
  }
}
// // We wrote some of the implementation for you
void gpio0__interrupt_dispatcher(void) {
  const int pin_interrupt = sw_pin_that_generated_interrupt();
  function_pointer_t attached_user_handler;
  if (LPC_GPIOINT->IO0IntStatR & (1 << pin_interrupt)) {
    attached_user_handler = gpio0_rising_edge_callbacks[pin_interrupt];
    fprintf(stderr, "SW2_P0.30\n");
  } else if (LPC_GPIOINT->IO0IntStatF & (1 << pin_interrupt)) {
    attached_user_handler = gpio0_falling_edge_callbacks[pin_interrupt];
    fprintf(stderr, "SW2_P0.29\n");
  }
  // Invoke the user registered callback, and then clear the interrupt
  attached_user_handler();
  LPC_GPIOINT->IO0IntClr |= (1 << pin_interrupt);
  // clear_pin_interrupt(pin_interrupt);
}
