#include "ssp2_lab.h"
#include "clock.h"
#include "lpc40xx.h"
#include "lpc_peripherals.h"
#include <stdint.h>

void ssp2_hw_init(uint32_t max_clock_mhz) {
  // Refer to LPC User manual and setup the register bits correctly
  // a) Power on Peripheral
  LPC_SC->PCONP |= (1 << 20);
  // b) Setup control registers CR0 and CR1
  LPC_SSP2->CR0 = 0b111 << 0;
  LPC_SSP2->CR1 = (1 << 1);
  // c) Setup prescalar register to be <= max_clock_mhz
  uint8_t divider = 2;
  const uint32_t cpu_clock_mhz = clock__get_core_clock_hz();

  // Keep scaling down divider until calculated is higher
  while (max_clock_mhz < (cpu_clock_mhz / divider) && divider <= 254) {
    divider += 2;
  }
  LPC_SSP2->CPSR = divider;
}
// void wait_till_data_tranfer_completes(void) {

// }

uint8_t ssp2_hw_exchange_byte(uint8_t data_out) {
  // Configure the Data register(DR) to send and receive data by checking the SPI peripheral status register
  LPC_SSP2->DR = data_out;
  while (LPC_SSP2->SR & (1 << 4)) {
    ; // keep it busy to complete the data transfer
  }
  // wait_till_data_tranfer_completes();

  return (uint8_t)(LPC_SSP2->DR & 0xFF);
}

uint8_t ssp2_hw_sr(uint8_t sr_data) {
  LPC_SSP2->DR = sr_data;
  return (uint8_t)(LPC_SSP2->SR & 0xFF);
}
